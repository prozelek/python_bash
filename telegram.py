import json
import urllib


class Telegram:
    ACCESS_KEY = '454008666:AAEusv5fFT3YWotoQhzTLcIP15mCWN35wNw'
    GET_UPDATES = 'getUpdates'
    SEND_MESSAGE = 'sendMessage'

    last_update_id = 0

    def get_telegram_request_url(self, function_name, parameters=None):
        params_string = ''
        if parameters is not None:
            params_string = '?' + urllib.urlencode(parameters)

        url = 'https://api.telegram.org/bot' + Telegram.ACCESS_KEY + '/' + function_name + params_string
        # print 'Requesting url ' + url
        return url

    def get_updates(self, callback):
        updates_data = self.get_data_from_telegram(self.GET_UPDATES, {'offset': self.last_update_id})
        for update_data in updates_data['result']:
            callback(update_data)
            if self.last_update_id <= update_data['update_id']:
                self.last_update_id = update_data['update_id'] + 1

    def get_data_from_telegram(self, function_name, parameters=None):
        req = urllib.urlopen(self.get_telegram_request_url(function_name, parameters))
        json_string = req.read()
        return json.loads(json_string)

    def send_data_to_telegram(self, function_name, parameters=None, data=None):
        data_string = urllib.urlencode(data)
        print 'Send data string' #+ data_string

        response = urllib.urlopen(self.get_telegram_request_url(function_name, parameters), data_string)
        response_string = response.read()
        # print 'Send data response ' + response_string

    def send_message(self, to, message_text):
        self.send_data_to_telegram(self.SEND_MESSAGE, None, {'chat_id': to, 'text': message_text})

