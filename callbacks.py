import commands
class Callbacks:
    telegram = None
    def __init__(self, telegram):
        self.telegram = telegram

    def updates_callback(self, data_item):
        print data_item
        try:
            message_text = data_item['message']['text']

            for known_command in commands.all_commands:
                if message_text == known_command.text_comm or message_text == known_command.text_comm + '@humpobot':
                    result_text = ''
                    if callable(known_command.text_out):
                        result_text = known_command.text_out(known_command())
                    else:
                        result_text = known_command.text_out
                    self.telegram.send_message(data_item['message']['chat']['id'], result_text)
        except Exception as e:
            print e
            
