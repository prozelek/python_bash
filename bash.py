# coding: utf-8

import math
import urllib
import numpy as np
import requests
import random
from HTMLParser import HTMLParser
max_id = 466909

#rnd = np.random
#rnd.seed(2)

class Bash:
    def getQuote(self, humourPower):
        i = 0
        while True:
            i = i + 1
            random.seed()
            quoteId = random.randint(1,max_id)
            url = 'http://bash.im/quote/' + str(quoteId)
            print 'getting ', url
            headers = {'Referer': 'http://bash.im/'}
            r = requests.get(url, headers=headers, allow_redirects=False)
            print r.status_code
            if r.status_code == 200:
                break
            if i > 50:
                return u'Не могу получить цитату\nПопробуй еще раз'.encode('utf-8')
        
        response = r.text
        quoteBegin = response.find('<div class="text">')
        quoteEnd = response.find('</div>', quoteBegin)
        if quoteBegin == -1 or quoteEnd == -1:
            print 'quote not found :('
            return 'quote not found :('
            
        quote = response[quoteBegin + len('<div class="text">'):quoteEnd]
        quote = quote.replace('<br>', '\n')
        quote = quote.replace('<br/>', '\n')
        quote = quote.replace('<br />', '\n')
        h = HTMLParser()
        quote = h.unescape(quote)
        
        #print "humourPower", humourPower
        #print quote.encode('utf-8')
        return quote.encode('utf-8')
        