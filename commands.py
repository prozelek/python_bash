#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bash

class Start:
    text_comm = "/start"
    text_out = "Приветствую тебя, путешественник!\nТы можешь написать мне /go, чтобы получить случайную цитату с bash.im\nНапиши /help, чтобы узнать больше."
    
class End:
    text_comm = "/finish"
    text_out = "До скорой встречи, путешественник!"

class Help:
    text_comm ="/help"
    text_out = "Введите /go, чтобы просмотреть случайную цитату."
    
class go:
    text_comm ="/go"
    bash = bash.Bash()
    text_out = (lambda self: self.bash.getQuote('infinite'))
    
all_commands = (Start, End, Help, go)