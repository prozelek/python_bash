import telegram
import callbacks
import time
import requests

telegram = telegram.Telegram()
callbacks_object = callbacks.Callbacks(telegram)

while True:
    try:
        telegram.get_updates(callbacks_object.updates_callback)
    except Exception as e:
        print str(e)
    time.sleep(1)
    
    